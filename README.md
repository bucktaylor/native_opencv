# native_opencv ## Detecting Edges

A flutter plugin project that incorporates OpenCV, an open-source Computer Vision Library built and optimized in C++.

The project includes an example application that runs on both Android and iOS. The example focuses on edge detection using several different methods. The image processing is all performed by OpenCV in a static library compiled for each platform and not performed at all by the customary native platform language such as Objective/C & Swift on iOS or Java & Kotlin on Android.

Flutter has the ability to call foreign functions (libraries) either to the native platform language using wrappers or to C directly using Foreign Function Interfaces (ffi). In this case, OpenCV is a C++ library, which is C compatible, so we can create C compliant interfaces to call the C++ functions.

## Information

Background information about OpenCV [Wikipedia OpenCV](https://en.wikipedia.org/wiki/OpenCV). The OpenCV [OpenCV Library Page](https://opencv.org/).

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

Flutter plug-in development can be found here:
[plug-in package](https://flutter.dev/developing-packages/).

Project repository [Gitlab Repository](https://gitlab.com/bucktaylor/native_opencv).

## Project Motivation

There are many platforms (hardware and OS) that users consume. Creating performant core libraries is often performed in C or C++, however these are typically not used by mobile development platforms (exception is QT). In business applications, writing code for mobile platforms potentially exposes proprietary code to savvy hackers who can easily open mobile application packages and inspect their contents. The idea of having faster code that is more difficult to inspect to common hackers motivated me to the idea if utilizing C compiled into bit-code libraries for use by Android or iOS applications.

I originally took the Mobile Track for iOS with the idea of utilizing C in mind. Upon completion, I also reviewed the Android track. After experiencing the marked differences between the two development platforms, I decided to look into the newer cross-platform Flutter, which is developed and supported by Goggle.

During my research I came across an OpenCV example which piqued my interest as it is a complex library and is also used for computer learning (and potentially AI). The example I found did not work as found (probably because of changes to the Flutter platform in just the few months between the example, and my discovery). I also found a "dev" package in teh Flutter repository which also packages the OpenCV library, but uses the "Wrapper" approach with Swift and Java instead of calling into C and performing the work in C/C++.

## Project Details

This application uses the OpenCV library (framework on iOS) and compiles the C++ code using custom CMAKE scripts for each platform. These libraries expose the OpenCV functionality by creating a custom C++ file which opens the subject image, performs the transformations/computations, and then writes it back to a temporary file. The function calls use external C visible attributes to the application can exchange (marshal) the primitive information types properly across the two different code containers.

Flutter uses the Dart programming language which has two ways of using Foreign Function Interfaces to inter-operate between languages. Dart can work with Objective-C, Swift, Java, Kotlin and C. The ability to "talk" to C, actually opens up the application to talk with many other languages which can use C compliant attributes to access this code (such as Rust).

There are many ways to process images on todays platforms. This project focuses solely on edge detection methods. It currently demonstrates 3 methods (still learning the UI interface widgets for Flutter).

## Related and/or Referenced Projects and Tutorials

[Integrating C library in a Flutter app using Dart FFI](https://medium.com/flutter-community/integrating-c-library-in-a-flutter-app-using-dart-ffi-38a15e16bc14).

[Using OpenCV in an iOS app](https://www.timpoulsen.com/2019/using-opencv-in-an-ios-app.html).

[Building a simple lane detection iOS app using OpenCV](https://medium.com/onfido-tech/building-a-simple-lane-detection-ios-app-using-opencv-4f70d8a6e6bc).

[Using FFI on Flutter Plugins to run native Rust code](https://medium.com/flutter-community/using-ffi-on-flutter-plugins-to-run-native-rust-code-d64c0f14f9c2).

[Using CMake on MacOS](https://medium.com/@divyendu.narayan/visual-studio-code-setup-in-mac-os-to-build-and-debug-c-cmake-projects-45a78b29e49).