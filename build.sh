printf "\nStarting build.sh\n"

export CC=clang
export CROSS_TOP=/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer
export CROSS_SDK=iPhoneOS.CROSS_SDK
export PATH="/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin:$PATH"

set -e

export CURDIR=$(pwd)
echo "CURDIR is: ${CURDIR}"
#ios
export INSTALL_DIR=${CURDIR}/build/ios #$(pwd/build) /platform/ios/native_opencv/Release/ios
echo "INSTALL_DIR is: ${INSTALL_DIR}"
mkdir -p build
#cd build

export IOS_TOOLCHAIN_FILE=${CURDIR}/ios.toolchain.cmake
echo "IOS_TOOLCHAIN_FILE is: ${IOS_TOOLCHAIN_FILE}"
export OPENCV_ROOT=$../ios/opencv2.framework
echo "OPENCV_ROOT is: ${OPENCV_ROOT}"

declare -a PLATFORMS=("OS" "OS64" "SIMULATOR" "SIMULATOR64" "OS64COMBINED")
PLATFORM="SIMULATOR64"

cd build && cmake -G "Xcode" -DENABLE_BITCODE=0 -DCMAKE_TOOLCHAIN_FILE=$IOS_TOOLCHAIN_FILE -DPLATFORM=$PLATFORM \
    -DCMAKE_BUILD_TYPE=Debug \
    -DLWS_WITH_LWSWS=0 \
    -DLWS_WITH_MBED=0 \
    -DLWS_WITHOUT_TESTAPPS=1 \
    -DLWS_OPENCV_INCLUDE_DIRS=${OPENCV_ROOT}/Headers

cmake --build . --config Debug --target install