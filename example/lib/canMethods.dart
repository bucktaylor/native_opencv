import 'package:native_opencv/native_opencv.dart';

class CanMethods {
  bool _findContours = false;
  bool _simpleCanny = false;
  bool _cannyBlur = false;
  bool _needSlider = false;
  C_Methods _selectedMethod = C_Methods.none;
  double _lowThreshold = 0;

  bool get isFindContours {
    return this._findContours;
  }

  set isFindContours(bool newValue) {
    this._findContours = newValue;
  }

  bool get isSimpleCanny {
    return this._simpleCanny;
  }

  set isSimpleCanny(bool newValue) {
    this._simpleCanny = newValue;
  }

  bool get isCannyBlur {
    return this._cannyBlur;
  }

  set isCannyBlur(bool newValue) {
    this._cannyBlur = newValue;
  }

  bool get needSlider {
    return this._needSlider;
  }

  set lowThreshold(double newThreshold) {
    this._lowThreshold = newThreshold;
  }

  double get lowThreshold {
    return this._lowThreshold;
  }

  C_Methods get selectedMethod {
    return _selectedMethod;
  }

  CanMethods() {
    // this._findContours = false;
    // this._simpleCanny = false;
    // this._cannyBlur = false;
  }

  void select(C_Methods cMethods, bool newValue) {
    if (newValue == false) {
      this._selectedMethod = C_Methods.none;
    } else {
      this._selectedMethod = cMethods;
    }

    switch (cMethods) {
      case C_Methods.findcontours:
        {
          this._findContours = newValue;
          this._simpleCanny = false;
          this._cannyBlur = false;
          this._needSlider = false;
        }
        break;
      case C_Methods.simplecanny:
        {
          this._findContours = false;
          this._simpleCanny = newValue;
          this._cannyBlur = false;
          this._needSlider = true;
        }
        break;
      case C_Methods.cannyblur:
        {
          this._findContours = false;
          this._simpleCanny = false;
          this._cannyBlur = newValue;
          this._needSlider = true;
        }
        break;
      case C_Methods.none:
        {
          this._findContours = false;
          this._simpleCanny = false;
          this._cannyBlur = false;
          this._needSlider = false;
        }
    }
  }

  static void process(ProcessImageArguments args) {
    switch (args.cMethod) {
      case C_Methods.findcontours:
        {
          findCountours(args);
        }
        break;
      case C_Methods.simplecanny:
        {
          cannyThreshold(args);
        }
        break;
      case C_Methods.cannyblur:
        {
          cannyWithBlur(args);
        }
        break;
      case C_Methods.none:
        {
          return;
        }
    }
  }
}
