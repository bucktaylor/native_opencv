import 'dart:async';
import 'dart:io';
import 'dart:isolate';

import 'package:flutter/material.dart';
//import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:native_opencv/native_opencv.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:package_info_plus/package_info_plus.dart';

import 'package:native_opencv_example/canMethods.dart';

final title = 'Native OpenCV Example';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: title,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //final Directory _tempDir = getTemporaryDirectory() as Directory;
  //Future<Directory?> _tempDir = getTemporaryDirectory();

  String _tempPath = "$getTemporaryDirectory().path/temp.jpg";
  // Making a variable to store a subscription in
  // ignore: cancel_subscriptions
  StreamSubscription? sub;
  ProcessImageArguments? _args;

  bool _isProcessed = false;
  bool _isWorking = false;
  File? _image;
  Image? _processedImage;
  CanMethods _canMethods = CanMethods();

  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  void showVersion(BuildContext context) {
    //final scaffoldState = Scaffold.of(context);
    final snackBar =
        SnackBar(content: Text('OpenCV version: ${opencvVersion()}'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    // scaffoldState
    //   ..removeCurrentSnackBar(reason: SnackBarClosedReason.dismiss)
    //   ..showSnackBar(snackbar);
  }

  void showPackageInfo(BuildContext context) {
    //final scaffoldState = Scaffold.of(context);
    final snackBar = SnackBar(
        content: Text(
      'App name: ${_packageInfo.appName}\n\n'
      'Package name: ${_packageInfo.packageName}\n'
      'App version: ${_packageInfo.version}\n'
      'Build number: ${_packageInfo.buildNumber}\n',
    ));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    // scaffoldState
    //   ..removeCurrentSnackBar(reason: SnackBarClosedReason.dismiss)
    //   ..showSnackBar(snackbar);
  }

  void initializeArgs() {
    _args = ProcessImageArguments('', _tempPath, 0, C_Methods.none);
  }

  void updateMethodSelector(
      BuildContext context, C_Methods cmethods, bool newvalue) {
    setState(() {
      _canMethods.select(cmethods, newvalue);
    });

    // if (_args == null) {
    //   initializeArgs();
    // }
    _args!.cMethod = _canMethods.selectedMethod;
    Navigator.pop(context);
    takeImageAndProcess();
  }

  void updateSlider(double newvalue) {
    // if (_args == null) {
    //   initializeArgs();
    // }
    _args!.lowThreshold = newvalue;
    takeImageAndProcess();
  }

  Future<void> _getImage(ImageSource imgtype) async {
    final picker = ImagePicker();
    _isProcessed = false;

    XFile? pickedFile = await picker.pickImage(source: imgtype);

    //setState(() {
    if (pickedFile != null) {
      _image = File(pickedFile.path);
      if (_args == null) {
        initializeArgs();
      }
      _args!.inputPath = _image!.path;
      takeImageAndProcess(); //args
    }
    //});
  }

  Future<void> takeImageAndProcess() async {
    //Make sure we have basic inputs for processing before calling ibrary

    if (_canMethods.selectedMethod == C_Methods.none ||
        _args!.inputPath == '') {
      return;
    }
    setState(() {
      if (_processedImage != null) {
        _processedImage!.image.evict();
      }
      _processedImage = Image.file(_image!);
    });

    // Creating a port for communication with isolate and arguments for entry point
    final port = ReceivePort();

    Isolate.spawn<ProcessImageArguments>(CanMethods.process, _args!,
        onError: port.sendPort, onExit: port.sendPort);

    setState(() {
      _isWorking = true;
    });

    // Listeting for messages on port
    sub = port.listen((_) async {
      setState(() {
        _processedImage = Image.file(
          File(_tempPath),
          alignment: Alignment.center,
        );
        _isProcessed = true;
        _isWorking = false;
      });
      // Cancel a subscription after message received called
      await sub!.cancel();
    });
  }

  @override
  void dispose() {
    super.dispose();
    sub!.cancel();
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
  }

  double _sldrVal = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        actions: <Widget>[
          Builder(
              builder: (context) => IconButton(
                    icon: Icon(Icons.more_vert),
                    onPressed: () => Scaffold.of(context).openEndDrawer(),
                  ))
        ],
      ),
      body: Stack(
        children: <Widget>[
          Center(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                if (_canMethods.selectedMethod == C_Methods.none)
                  Text(
                    'Select Edge detection method',
                    textAlign: TextAlign.center,
                  ),
                ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: 3000, maxHeight: 400),
                  child: (_image != null && _isProcessed)
                      ? _processedImage
                      : Center(
                          child: Text(
                          'No image selected...',
                          textAlign: TextAlign.center,
                        )),
                ),
                Padding(
                    padding: EdgeInsets.all(20),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          ElevatedButton(
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.image),
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(10, 0, 20, 0),
                                    child: Text('Use Gallery'),
                                  ),
                                ],
                              ),
                              onPressed: () => _getImage(ImageSource.gallery)),
                          ElevatedButton(
                              child: Row(
                                children: [
                                  Icon(Icons.camera),
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(10, 0, 20, 0),
                                    child: Text('Use Camera'),
                                  )
                                ],
                              ),
                              onPressed: () => _getImage(ImageSource.camera)),
                        ])),
                if (_canMethods.needSlider)
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: <Widget>[
                        Text('Current value: ' +
                            _canMethods.lowThreshold.round().toString()),
                        Slider(
                          value: _sldrVal,
                          onChangeStart: (double newValue) {
                            print('On-Start value: $newValue');
                          },
                          onChanged: (double newValue) {
                            setState(() {
                              _sldrVal = newValue;
                            });
                          },
                          onChangeEnd: (double newValue) {
                            print('On-End value: $newValue');
                            _canMethods.lowThreshold = newValue;
                            updateSlider(newValue);
                          },
                          min: 0.0,
                          max: 100.0,
                          divisions: 20,
                          label: _sldrVal.round().toString(),
                        ),
                        Text('Set the Threshold value using slider')
                      ],
                    ),
                  )
              ],
            ),
          ),
          if (_isWorking)
            Positioned.fill(
                child: Container(
              color: Colors.black.withOpacity(.7),
              child: Center(child: CircularProgressIndicator()),
            )),
        ],
      ),
      drawer: Drawer(
          child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            padding: EdgeInsets.all(30),
            //margin: EdgeInsets.all(20),
            child: Column(children: <Widget>[
              Icon(Icons.ac_unit_outlined),
              Divider(
                height: 20,
                thickness: 4,
              ),
              Text('Select Edge Detection Mode'),
            ]),
            decoration: BoxDecoration(color: Colors.lightBlueAccent),
          ),
          ListTile(
            leading: Icon(Icons.crop_original),
            title: Text('Box with Contours'),
            trailing: Checkbox(
              value: _canMethods.isFindContours,
              onChanged: (bool? newvalue) => {
                updateMethodSelector(context, C_Methods.findcontours, newvalue!)
              },
            ),
            onTap: () => updateMethodSelector(
                context, C_Methods.findcontours, !_canMethods.isFindContours),
          ),
          ListTile(
            leading: Icon(Icons.border_horizontal),
            title: Text('Basic Canny Method'),
            trailing: Checkbox(
              value: _canMethods.isSimpleCanny,
              onChanged: (bool? newvalue) => updateMethodSelector(
                  context, C_Methods.simplecanny, newvalue!),
            ),
            onTap: () => updateMethodSelector(
                context, C_Methods.simplecanny, !_canMethods.isSimpleCanny),
          ),
          ListTile(
            leading: Icon(Icons.blur_circular),
            title: Text('Canny with Blur Method'),
            trailing: Checkbox(
              value: _canMethods.isCannyBlur,
              onChanged: (bool? newvalue) =>
                  updateMethodSelector(context, C_Methods.cannyblur, newvalue!),
            ),
            onTap: () => updateMethodSelector(
                context, C_Methods.cannyblur, !_canMethods.isCannyBlur),
          )
        ],
      )),
      endDrawer: Drawer(
          child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text('Information'),
            decoration: BoxDecoration(color: Colors.amber),
          ),
          Builder(
              builder: (context) => ListTile(
                    title: Text('OpenCV Version'),
                    onTap: () {
                      showVersion(context);
                      Navigator.pop(context);
                    },
                  )),
          Builder(
              builder: (context) => ListTile(
                    title: Text('Flutter Package'),
                    onTap: () {
                      showPackageInfo(context);
                      Navigator.pop(context);
                    },
                  )),
        ],
      )),
    );
  }
}
