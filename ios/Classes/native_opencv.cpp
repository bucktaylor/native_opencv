
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <chrono>
//#include <iostream>

#ifdef __ANDROID__
#include <android/log.h>
#endif

using namespace cv;
using namespace std;

long long int get_now() {
    return chrono::duration_cast<std::chrono::milliseconds>(
            chrono::system_clock::now().time_since_epoch()
    ).count();
}

void platform_log(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
#ifdef __ANDROID__
    __android_log_vprint(ANDROID_LOG_VERBOSE, "ndk", fmt, args);
#else
    vprintf(fmt, args);
#endif
    va_end(args);
}

// Avoiding name mangling
extern "C" {
    // Attributes to prevent 'unused' function from being removed and to make it visible
    __attribute__((visibility("default"))) __attribute__((used))
    const char* version() {
        return CV_VERSION;
    }

    __attribute__((visibility("default"))) __attribute__((used))
    void find_contours(char* inputImagePath, char* outputImagePath) {
        long long start = get_now();
        
        Mat input = imread(inputImagePath, IMREAD_GRAYSCALE);
        Mat threshed, withContours;

        vector<vector<Point>> contours;
        vector<Vec4i> hierarchy;
        
        adaptiveThreshold(input, threshed, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 77, 6);
        findContours(threshed, contours, hierarchy, RETR_TREE, CHAIN_APPROX_TC89_L1);
        
        cvtColor(threshed, withContours, COLOR_GRAY2BGR);
        drawContours(withContours, contours, -1, Scalar(0, 255, 0), 3);
        
        imwrite(outputImagePath, withContours);

        int evalInMillis = static_cast<int>(get_now() - start);
        platform_log("Processing done in %dms\n", evalInMillis);
    }

    __attribute__((visibility("default"))) __attribute__((used))
    void cannythreshold(char* inputImagePath, char* outputImagePath, double lowThreshold) {
        long long start = get_now();

        //int lowThreshold = 30;
        //const int max_lowThreshold = 100;
        const int ratio = 3;
        const int kernel_size = 3;

        Mat src = imread(inputImagePath, IMREAD_COLOR);
        Mat src_gray;
        Mat dst, detected_edges;

        dst.create(src.size(), src.type());
        
        cvtColor(src, src_gray, COLOR_BGR2GRAY);

        Canny(src_gray, dst, lowThreshold, (lowThreshold * ratio), kernel_size, true);

        imwrite(outputImagePath, dst);

        int evalInMillis = static_cast<int>(get_now() - start);
        platform_log("Processing done in %dms\n", evalInMillis);
    }

    __attribute__((visibility("default"))) __attribute__((used))
    void cannywithblur(char* inputImagePath, char* outputImagePath, double lowThreshold) {
        long long start = get_now();

        //int lowThreshold = 30;
        //const int max_lowThreshold = 100;
        const int ratio = 3;
        const int kernel_size = 3;

        Mat src = imread(inputImagePath, IMREAD_COLOR);
        Mat src_gray;
        Mat dst, detected_edges;

        dst.create(src.size(), src.type());
        
        cvtColor(src, src_gray, COLOR_BGR2GRAY);
        blur(src_gray, detected_edges, Size(3,3));
        Canny(detected_edges, detected_edges, lowThreshold, (lowThreshold * ratio), kernel_size, true);
        dst = Scalar::all(0);
        src.copyTo(dst, detected_edges);

        imwrite(outputImagePath, dst);

        int evalInMillis = static_cast<int>(get_now() - start);
        platform_log("Processing done in %dms\n", evalInMillis);
    }
}