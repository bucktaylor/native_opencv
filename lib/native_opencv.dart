import 'dart:ffi' as ffi;
import 'dart:io';
//import 'dart:async';
import 'package:ffi/ffi.dart';
import 'package:flutter/services.dart';

// C function signatures
typedef VersionNative = ffi.Pointer<Utf8> Function();
typedef FindContoursNative = ffi.Void Function(
    ffi.Pointer<Utf8>, ffi.Pointer<Utf8>);
typedef CannyThresholdNative = ffi.Void Function(
    ffi.Pointer<Utf8>, ffi.Pointer<Utf8>, ffi.Double);
typedef CannyWithBlurNative = ffi.Void Function(
    ffi.Pointer<Utf8>, ffi.Pointer<Utf8>, ffi.Double);

// Dart function signatures
typedef _VersionFunc = ffi.Pointer<Utf8> Function();
typedef _FindContours = void Function(ffi.Pointer<Utf8>, ffi.Pointer<Utf8>);
typedef _CannyThreshold = void Function(
    ffi.Pointer<Utf8>, ffi.Pointer<Utf8>, double);
typedef _CannyWithBlur = void Function(
    ffi.Pointer<Utf8>, ffi.Pointer<Utf8>, double);

// Getting a library that holds needed symbols
final ffi.DynamicLibrary _lib = Platform.isAndroid
    ? ffi.DynamicLibrary.open('libnative_opencv.so')
    : ffi.DynamicLibrary.process();

// Looking for the functions
final _VersionFunc _version =
    _lib.lookup<ffi.NativeFunction<VersionNative>>('version').asFunction();

final _FindContours __findContours = _lib
    .lookup<ffi.NativeFunction<FindContoursNative>>('find_contours')
    .asFunction();

final _CannyThreshold __cannyThreshold = _lib
    .lookup<ffi.NativeFunction<CannyThresholdNative>>('cannythreshold')
    .asFunction();

final _CannyWithBlur __cannyWithBlur = _lib
    .lookup<ffi.NativeFunction<CannyWithBlurNative>>('cannywithblur')
    .asFunction();

String opencvVersion() {
  //return Utf8.fromUtf8(_version());
  return _version().toDartString();
}

void findCountours(ProcessImageArguments args) {
  //__findContours(Utf8.toUtf8(args.inputPath), Utf8.toUtf8(args.outputPath));
  __findContours(args.inputPath.toNativeUtf8(), args.outputPath.toNativeUtf8());
}

void cannyThreshold(ProcessImageArguments args) {
  // __cannyThreshold(
  //   Utf8.toUtf8(args.inputPath),
  //   Utf8.toUtf8(args.outputPath),
  //   args.lowThreshold,
  // );
  __cannyThreshold(
    args.inputPath.toNativeUtf8(),
    args.outputPath.toNativeUtf8(),
    args.lowThreshold,
  );
}

void cannyWithBlur(ProcessImageArguments args) {
  // __cannyWithBlur(
  //   Utf8.toUtf8(args.inputPath),
  //   Utf8.toUtf8(args.outputPath),
  //   args.lowThreshold,
  // );
  __cannyWithBlur(
    args.inputPath.toNativeUtf8(),
    args.outputPath.toNativeUtf8(),
    args.lowThreshold,
  );
}

//Methods to compute edges
enum C_Methods { none, findcontours, simplecanny, cannyblur }

class ProcessImageArguments {
  String inputPath;
  String outputPath;
  double lowThreshold;
  C_Methods cMethod;

  ProcessImageArguments(
      this.inputPath, this.outputPath, this.lowThreshold, this.cMethod);
}

class NativeOpencv {
  static const MethodChannel _channel = const MethodChannel('native_opencv');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
